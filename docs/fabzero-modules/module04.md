# 4. Outil Fab Lab sélectionné
L'outil que j'ai choisi est le microcontrolleur. Les microcontrolleurs sont des microcomputers qui permettent 
de contrôler des dispositifs électronique et des systèmes de manière 
autonomes. Cela étant possible grâce à sa capacité d'exécuter des codes qui contrôlent ces systèmes pour une 
collection de données ou aussi faire des actions à partir de ces données.  
Il me semblait compliqué mais j'aime bien les challenges et au même temps 
je le trouvais très intéressant. On a reçu un kit avec un microntrolleur, un câble à porte USB pour le connecter avec notre 
pc et pleins d'autres choses. Je vous avoue que la plupart de ces élements étaient inconnus pour moi, mais on verra par la suite.  
Le microcontrolleur est un RP2040 qui ressemble au Rpi Pico.  
![](images/rp_map.png)  
Voilà la mappe des sorties de cette appareil.  
Les quatre élements à maitriser pour l'instant sont le SDA (Serial Data Line), SCL (Serial Clock Line), la sorti d'alimentation (3V3) et la terre (GND). Les deux derniers nous 
permettrant de construire un circuit qui alimente tout ce qu'on va connecter avec l'appareil. Le SDA et le SCL étant les signaux utilsé par les systèmes I2C et permettent le 
transfer de data.
On commence par le système de communication avec le microcontrolleur. Personnellement j'ai choisi de travaillé avec micropython 
étant donné que j'avais déjà Thonny installé dans mon pc et que je me trouve plus à l'aise avec le language Python. Ayant déjà Thonny 
il me fallait juste ouvrir Thonny, aller sur **tools** -> **options** -> **interpreter** et à la question "Which interpreter or device should Thonny use 
for running your code?" mettre "MicroPython (Rasperry Pi Pico)". On installe le paquet et on connecte notre appareil. 
Pour ce faire il faudra le brancher avec le cable à porte USB avec notre ordinateur tout en restant appuyé sur le bouton "boot" du 
microcontrolleur.  
![](images/boot.jpg)  
Fermez et re-ouvrez Thonny maintenant que vous avez branché l'RP2040 et ouvrez un nouveau fichier. 
Si tout s'est bien passé, vous êtes censé vous retrouver avec ça  
![](images/rp.png)  
On clique sur la deuxième option et on se retrouve dans notre microntrolleur. Dans mon cas il y avait déjà ce code  
```
import machine
import utime

import array, time
from machine import Pin
import rp2
from rp2 import PIO, StateMachine, asm_pio
# LED引脚
led_onboard = machine.Pin(25, machine.Pin.OUT)


# Configure the number of WS2812 LEDs.
NUM_LEDS = 1

@asm_pio(sideset_init=PIO.OUT_LOW, out_shiftdir=PIO.SHIFT_LEFT, autopull=True, pull_thresh=24)
def ws2812():
    T1 = 2
    T2 = 5
    T3 = 3
    label("bitloop")
    out(x, 1)               .side(0)    [T3 - 1] 
    jmp(not_x, "do_zero")   .side(1)    [T1 - 1] 
    jmp("bitloop")          .side(1)    [T2 - 1] 
    label("do_zero")
    nop()                   .side(0)    [T2 - 1]
    
# Create the StateMachine with the ws2812 program, outputting on Pin(23).ws2812引脚
sm = StateMachine(0, ws2812, freq=8000000, sideset_base=Pin(23))

# Start the StateMachine, it will wait for data on its FIFO.
sm.active(1)

# Display a pattern on the LEDs via an array of LED RGB values.
ar = array.array("I", [0 for _ in range(NUM_LEDS)])

time.sleep_ms(20)

print("red")
for j in range(0, 20): 
    for i in range(NUM_LEDS): 
        ar[i] = j<<8
    sm.put(ar,8) 
    time.sleep_ms(50)
   
print("green")
for j in range(0, 20): 
    for i in range(NUM_LEDS): 
        ar[i] = j<<16 
    sm.put(ar,8) 
    time.sleep_ms(50)
    
print("blue")
for j in range(0, 100): 
    for i in range(NUM_LEDS): 
        ar[i] = j 
    sm.put(ar,8) 
    time.sleep_ms(5)
print("white")
# for j in range(0, 100):
#     for i in range(NUM_LEDS): 
#         ar[i] = j<<16 + j<<8 + j 
#     sm.put(ar,8) 
#     time.sleep_ms(100)   
for i in range(NUM_LEDS): 
    ar[i] = 990000
    sm.put(ar,8) 
     
while True: 
    led_onboard.value(1) 
    utime.sleep(3) 
    led_onboard.value(0) 
    utime.sleep(.1)
```  
Ceci nous permettra de chipoter avec le LED en sorti GP23 (allez voir dans la mappe) et avec le mini LED en GP25 (representé 
par "led_onboard") dans le code.  


## Exercise RGB
Le premier exercise sera basé sur la création d'un code qui nous permettra d'allumer et éteindre le LED en GP23. Pour ce faire 
j'ai écrit ce code:  

```
import machine
import neopixel
import utime

# Configure the pin and number of LEDs
pin = machine.Pin(23)
num_leds = 1

# Create a neopixel object with the pin and number of LEDs
np = neopixel.NeoPixel(pin, num_leds)

# Define the colors and on/off times
colors = [(255, 0, 0), (255, 255, 0), (0, 255, 0), (0, 255, 255), (0, 0, 255), (255, 0, 255)]
on_time = 1  # seconds
off_time = 0.5  # seconds

# Loop through the colors and turn the LED on and off
while True:
    for color in colors:
        # Turn the LED on with the current color
        np[0] = color
        np.write()

        # Wait for the on time
        utime.sleep(on_time)

        # Turn the LED off
        np[0] = (0, 0, 0)
        np.write()

        # Wait for the off time
        utime.sleep(off_time)


```  
Ce code nous permettra d'allumer (pendant une seconde) et éteindre (pendant 0,5 sencondes) en boucle le LED en sorti 23 
avec différents couleurs: rouge (255,0,0), jaune (255,255,0), vert (0,255,0), cyan (0,255,255), bleue (0,0,255) et magenta (255,0,255).  
Pour ce qui est les **modules** on a utilisé:  
- **machine** qui nous donne accès à l'hardware de notre microcontrolleur;  
- **neopixel** qui nous une iterface facile à utiliser pour controller le RGB LED;  
- **time** qui nous donne des fonctions liés au temps.  
On a defini cinq variables: 
- le pin du LED;  
- le nombres de LED;  
- les couleurs qu'on veut utilisés;  
- le temps que le LED reste allumé;  
- le temps que le LED reste éteint;  
- np (pour NeoPixel) qui nous permettra de controller le LED et pour ce faire il faut mettre deux arguments telle que le pin et le nombre de LEDs.
Maintenant on peut definir une boucle, qu'on nomme cycle(), qui nous permet d'allumer et éteindre ce LED tout en changeant de couleur à 
chaque fois qu'on y rentre.  

## temperature et humidité  

![](images/dht20.png)

Pour utiliser le [DHT20](http://www.aosong.com/en/products-67.html), qui est un capteur de temperature et humidité relative 
(RH), il faudra aller dans micropython et faire le copier-coller du code que vous retrouverez [ici](https://github.com/flrrth/pico-dht20).
Ce code sera mit dans le main.py et on ouvrira aussi une autre page dans thonny (ctrl+N) et où on mettra le code [ci-joint](https://github.com/flrrth/pico-dht20/blob/main/dht20/dht20.py). 
Il faudra sauvegarder celui-là comme "dht20.py", toujours dans le microntrolleur (ctrl+S -> rasperry pi pico).  
Revenant à l'appareil, il présente quattre pattes: SCL, SDA, VDD et GND. On les connait déjà et on suit le schéma ci dessous pour brancher les différentes parties 
(petit conseil: faites le toujours sans qu'il y ait du courant qui y passe, pour eviter des court circuits) 

![](images/dht.png)  

Maintenant que le branchage a été fait, on peut reouvrir Thonny et quand on revient au **main.py**, on peut faire partir le code et on verra des valeurs données chaque seconde de la 
temperature et de l'humidité relative. On peut même faire un test et souffler sur le capteur et on remarquera que les valeurs de l'humidité 
augmentent pendant qu'on souffle comme vous pouvez voir dans cette petite [vidéo](https://www.youtube.com/shorts/L-4bLCXrBHE). 
Si vous êtes à l'ombre vous pouvez aussi placer l'appareil sous la lumière et voir que la temperature augmente. 
Mais sachez que les valeurs sont précises à 0,05 près selon le [datasheet](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf) et aussi qu'il faut que le voltage qui y passe 
soit en dessous de 5,5 volt (ne vous inquietez pas, si vous le branchez à l'ordinateur vous allez pas à devoir mettre des resistances).  

## Mpu6500
Le MPU-6500 est un SiP (Système in Package) qu'on peut retrouver dans des drones et robots permettant de faire des mésures d'acceleration, de 
vitesse angulaire et de temperature. Pour cela j'ai demandé à chatgpt un code pour utilisé le SiP, cela étant le résultat:
```
import machine
import time

# MPU6500 register addresses
MPU6500_I2C_ADDR = 0x68
MPU6500_PWR_MGMT_1 = 0x6B
MPU6500_GYRO_CONFIG = 0x1B
MPU6500_GYRO_XOUT_H = 0x43

# Initialize I2C bus
i2c = machine.I2C(0, scl=machine.Pin(5), sda=machine.Pin(4), freq=400000)

# Reset MPU6500 and configure clock source
i2c.writeto_mem(MPU6500_I2C_ADDR, MPU6500_PWR_MGMT_1, b'\x80')
time.sleep_ms(100)
i2c.writeto_mem(MPU6500_I2C_ADDR, MPU6500_PWR_MGMT_1, b'\x01')
time.sleep_ms(100)

# Configure gyroscope range (+/- 2000 degrees/sec)
i2c.writeto_mem(MPU6500_I2C_ADDR, MPU6500_GYRO_CONFIG, b'\x18')

while True:
    # Read gyroscope data
    gyro_raw = i2c.readfrom_mem(MPU6500_I2C_ADDR, MPU6500_GYRO_XOUT_H, 6)
    
    # Convert raw data to degrees/sec
    gyro_x = (gyro_raw[0] << 8 | gyro_raw[1]) / 131.0
    gyro_y = (gyro_raw[2] << 8 | gyro_raw[3]) / 131.0
    gyro_z = (gyro_raw[4] << 8 | gyro_raw[5]) / 131.0
    
    # Print gyroscope data
    print("Gyroscope (degrees/sec): X=%.2f Y=%.2f Z=%.2f" % (gyro_x, gyro_y, gyro_z))
    
    # Wait for 100 milliseconds
    time.sleep_ms(100)
	
```  
Par contre j'ai remarqué que ce code ne donnait que l'acceleration, mais je savais que l'MPU-6500 peut aussi donner la temperature et la vitesse angulaire, 
donc j'ai demandé à chatgpt d'écrire un code qui me permette d'obtenir tous ces informations. Et voilà le nouveau code (j'ai changé la valeur 
la fonction "timesleep" de 0.1 à 1, pour avoir le temps de lire les valeurs):  
```
import machine
import time

# MPU6500 register addresses
MPU_ADDR = 0x68
WHO_AM_I = 0x75
PWR_MGMT_1 = 0x6B
GYRO_CONFIG = 0x1B
ACCEL_CONFIG = 0x1C
ACCEL_XOUT_H = 0x3B
GYRO_XOUT_H = 0x43
TEMP_OUT_H = 0x41

# Initialize I2C interface
i2c = machine.I2C(0, scl=machine.Pin(9), sda=machine.Pin(8), freq=400000)

# Read WHO_AM_I register to confirm connection
whoami = i2c.readfrom_mem(MPU_ADDR, WHO_AM_I, 1)
if whoami[0] != 0x70:
    raise ValueError("MPU6500 not found on I2C bus")

# Configure MPU6500
i2c.writeto_mem(MPU_ADDR, PWR_MGMT_1, b'\x00')  # Wake up MPU6500
i2c.writeto_mem(MPU_ADDR, GYRO_CONFIG, b'\x08')  # Set gyroscope full scale range to +/- 500 degrees/s
i2c.writeto_mem(MPU_ADDR, ACCEL_CONFIG, b'\x08')  # Set accelerometer full scale range to +/- 4g

# Read data from MPU6500
while True:
    data = i2c.readfrom_mem(MPU_ADDR, ACCEL_XOUT_H, 14)
    x_accel_raw = (data[0] << 8) | data[1]
    y_accel_raw = (data[2] << 8) | data[3]
    z_accel_raw = (data[4] << 8) | data[5]
    x_gyro_raw = (data[8] << 8) | data[9]
    y_gyro_raw = (data[10] << 8) | data[11]
    z_gyro_raw = (data[12] << 8) | data[13]
    temp_raw = (data[6] << 8) | data[7]

    # Convert raw data to values
    x_accel_mss = x_accel_raw / 16384.0
    y_accel_mss = y_accel_raw / 16384.0
    z_accel_mss = z_accel_raw / 16384.0
    x_gyro_dps = x_gyro_raw / 131.0
    y_gyro_dps = y_gyro_raw / 131.0
    z_gyro_dps = z_gyro_raw / 131.0
    temp_celsius = (temp_raw / 333.87) + 21.0

    # Print sensor data
    print("Accelerometer (m/s^2): X=%.2f Y=%.2f Z=%.2f" % (x_accel_mss, y_accel_mss, z_accel_mss))
    print("Gyroscope (°/s): X=%.2f Y=%.2f Z=%.2f" % (x_gyro_dps, y_gyro_dps, z_gyro_dps))
    print("Temperature (°C): %.2f" % temp_celsius)

    time.sleep(1)  # Wait 1000 ms before reading again
```  
![](images/mpu.png)  

L'image ci-dessus étant le résultat de ce code. Je vous ai souligné trois moments différents qui ont été obtenus par le MPU-6500: 
le carré rouge représentant les dernière données et le vert celles de deux secondes avant. J'ai remarqué que il y a 
des fluctuations importantes de valeurs pour la vitesse angulaire selon Y, ce qui me parassait curieux donc j'ai demandé à chatgpt 
les possibles motivations et celle qui me parassait la plus plausible est que l'appareil est très sensible et que des fluctuations 
de temperature (moins plausible étant donné que la temperature reste assez contante) et des interférences electromagnetiques 
pourraient affecté sur le captages d'informations par le senseur. Une motivation plausible pourrait être aussi que des bruits produits 
par le capteur pourraient affecter sur l'aquisitions des données.  

#Checklist des tâches du module  

Voilà la checklist finale:  

- [x] documente what you have learned from interfacing input devices to a micro controller and how the physical property relates to 
the measured results  
- [x] programme your board  
- [x] describe your programming process  
- [x] outline problems and how you fixed them  
- [x] include original design files and source code  