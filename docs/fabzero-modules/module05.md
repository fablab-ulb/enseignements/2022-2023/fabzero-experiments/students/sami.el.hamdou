# 5. Dynamique de groupe et projet final

On est arrivé au cinquième module où on commence à proprement parler du projet final et comment on y arrive.  
Pour commencer il faudra maitriser la manière d'analiser des problèmes. Normalement les gens pensent à 
une problèmatique et commencent à chercher une solution. Mais souvent ils trouvent une solution qui ne va pas proprement 
traiter le problème ou sinon ils trouvent un projet à faire mais qui ne resout aucun problème. 
Dès qu'on a compris comment destructurer une problématique, on peut arriver à la conception de notre projet analysant 
chaque "cause" de la problématique et cherchant à resoudre une de ces causes. Le but n'est pas de révolutionner le 
monde réalisant un projet qui resout tous les problèmes mais plutôt de trouver plusieurs petites solutions 
qui ensemble vont tout changer. Donc on va faire une analise profonde avec un binome pour s'exercer.  
Avec [Kamel Sanou](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/) j'ai fait 
un arbre qui aborde le sujet de la surpopulation. 

## La surpopulation  

### Problème

Le 15 Novembre 2022 a été estimé que la population mondiale ait touché les huit miliards de gens, ce qui a des impacts à la 
fois sociétaux et environnementaux, que je represente dans l'image ci-dessous 

![](images/problématique.png)

On a trouvé ce sujet d'importance non-negligeable car dans les dernièrs 400 ans il y a eu une augmentation démographique 
exponentielle, à savoir que juste en 1700 il y avait 679 milions d'habitants, en 1800 le nombre est passé à 1,250 miliards, 
ensuit à 1,76 miliards en 1900 pour enfin arriver à 6,1 miliards d'habitants dans les années 2000.  
Cette augmentation mène à un élargissement de la société causant les déforestions, dû aux besoins de construire des nouveaux 
logements, mais aussi pour une exploitation agricole afin de nourrir suffisamment de gens. La pratique agricole intensive dans 
cette exploitation entraîne un appauvrissement des terres et une diminution de leurs fertilité. La 
déforestation entraine aussi à une augmentation des **espèce en voie de disparition** et pas seulement à cause du 
retressissement de leurs habitats mais aussi par la chasse et la **pollution** des milieux, qui vient des pots d'échappement 
des voitures. À propos des voitures, les **embouteillages** sont un gros problèmes: il faut toujours beaucoup plus de temps pour 
arriver à notre destination, ça craint on est d'accord?  
Revenant à notre sujet, cette pollution est aussi la motivation pour laquelle on lutte tous les jours contre les **effets de 
serres** excessifs dans le monde, ce qui est à la base de l'augmentation de la temperature globale et du coup de la fonte des glaciers 
avec la montée du niveau de la mer. Pour terminer il y a énormement de gaspillage de nurriture et avec le nombre de gens qui 
augmente, la quantité de gaspillage augmente à son tour aussi.  
Mais quelles sont les causes à cette hausse demographique?  
- Le planning familiale: selon wikipedia "c'est l'ensemble des moyens qui concourent au contrôle des naissances", 
ce qui signifie qu'il y a des système qui permettent à la fois d'informer les gens sur ce qui comporte avoir des 
enfants dans tous les aspects (souvent les gens ne sont pas au courant des coûts qui leurs attendent après avoir 
eu un ou plusieurs enfants), mais que personne n'en profite;  
- L'evolution des systèmes sanitaires a eu deux effets: diminution du taux de mortalité et augmentation du taux 
de natalité. Les deux sont aussi lié car le prémier point implique qu'il y a plus de gens qui arrivent à 
maturité pour se reproduire ce qui engendre à une ultérieure augmentation de natalité;  
- Le mode de vie: les deux cas extrèmes portent à avoir plus de gens qui se reproduisent. Le gens qui se 
retrouvent pauvre le font pour avoir plus de mains qui aident la famille; les gens qui mènent 
une vie aisé se retrouvent à avoir plus d'enfants parce que leur situation financière le permet.  

### Solution  

![](images/solution.png)  
Première solution: on utilise le claquement de doigts de Thanos pour baisser le nombre de gens à moitié. Simple 
et efficace.  
Ce qui est un peu plus faisable c'est d'informer les gens dès les secondaires. Effectivement aux États-Units les 
écoles donnent des cours sur l'éducation sexuelle, ce qui limite le nombre d'adolescents qui finissent par avoir 
des enfants.  
En Chine il existait un système qui limitait le nombre de fils qu'une famille pouvait avoir grâce au "One-child 
policy" qui a été instauré en 1979 jusqu'au 2015. Selon cette loi les familles pouvaient avoir un seul fils et 
dans le cas ils avaient une fille ou un fils avec un handicap, ils pouvaiaent en avoir un autre. Cette loi a été 
mit en conséquence de la surpopulation chinoise qui allait avoir des impacts importants sur l'économie du pays. 
Le résultat étant bien une baisse demographique du pays mais ayant aussi des effets secondaires telle qu'un 
déséquilibre entre le nombre de gens agées et jeunes. Il ne faut pas oublié les incidences sur le droit humain: 
les gens étaient obligé à faire des abortions ou se faire stériliser pour éviter des sanction qui 
pouvaient être plus ou moins sévères. Ce système était efficace mais trop strict, il faudrait peut être simplement 
ajouter des taxes qui augmentent avec l'augmentation du nombre d'enfants que la famille a. Avec cette argent ça 
serait possible d'aider financièrement ceux qui par contre ont besoin de plusieurs enfants pour un coup de main à 
la famille à cause de leur situation économique compliquée.  
Il existe aussi un nombre élevé d'enfants qui se retrouvent sans parents alors qu'ils sont des être humains qui 
ont besoin de grandir dans un environnement où ils peuvent être aimé et se sentir protegés. L'adoption aussi est 
une solution qui limiterais le nombre de naissances dans le monde tout en donnant une opportunité à des orphelins 
d'avoir tout ce qui est considéré comme acquis pour beaucoup d'autres gens.  


# Formation des groupes

Passons au jour de la formation des groupes où il fallait ramener un objet qui represente une problématique qui 
nous tiens à coeur. Pour ce faire, j'ai ramener la coque de mon gsm qui est arrivé au bout de sa vie franchement lol.  
Par contre c'était cohérent avec la problématique qui plus m'intéresse: le fait que nos objet de tous les jours 
deviennent trop facilement des dêchets qui s'accumule dans notre ecosystème, au niveau macroscopique 
(bout de plastiques, boutèilles, sacs, etc.) ou microscopique (par exemple les microplastiques qui rentrent dans la chaîne alimentaire 
s'accumulent dans les êtres vivants). Par rapport à ce sujet il y aurait des tonnes de choses à dire mais ce n'est pas ça le but de ce 
module. Ma coque fait partie de cette problématique: il est temps de s'en débarasser et si je le fais probablement cet 
objet pourrait se retrouver dans le [Great Pacific Garbage Patch](https://education.nationalgeographic.org/resource/great-pacific-garbage-patch/) 
dans même pas un an.  
On reviens à la formation des groupes pour notre projet final. Il nous a été demandé de déposer l'objet qu'on a 
ramené, au milieu de la salle et faire un tour pour voir si il y a des objets qui pourraient se rapprocher à la 
problématique qui nous intéresse pour ensuite former un groupe de quattre avec les propriétaires de ces objets. Finalement 
je me suis retrouvé en groupe avec [Karl](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/karl.preux/), 
[Ibrahima](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/ibrahima.bah) 
et [Victor](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/karl.preux/). Un groupe 
qui partage un intéret à la recherche d'une solution aux **dêchets** dans le monde mais aussi pour le basket. 

## Thématique  
La prémière étape consistait à écrire sur une grande feuille tous les problèmes qu'on pouvait penser et qui sont liés aux 
objets ramener. Ci-dessous vous pouvez voir le résultat de ce petit brainstorming  
![](images/brainstorming.jpg)  
Finalement on s'est rendu compte que tout était plus ou moins centré sur la pollution.  

## Problématique  
Maintenant on peut se concentrer sur les problématiques pour ensuite se converger sur une en particulier. Pour ce faire 
on a écrit une liste de problématiques:  
- on a besoin de matériaux prémiers qui viennent de différents pays pour la production dans les industries;  
- ces dêchets finissent par s'accumuler dans les océans et former des îles de dêchet comme j'ai déjà dit précédemment;  
- on génére beaucoup de dêchets dans la vie de tous les jours;  
- les gens ne sont pas assez sensibilisé sur les conséquences de cette pollution.  
À partir de ces points il faut trouver une problématique plus concise qui regroupe au mieux ces points et on a reformulé 
la problématique comme: comment réduire la surconsommation de matières prémieres à partir des dêchets.  
Maintenant il faudra penser au dêchet en question et on a pensé aux canettes, bics, boîte à pizza usées, marc de café, 
emballage en plastique et anciènnes chaussures.  
Au final on a opté pour les marc à café et on a construit nos arbres sur cette base  
![](images/arbre.jpg)  
