# 2. Computer Aided Design (CAD)  

Dans ce module on apprendra à utiliser des logiciels nous permettant de faire de la modélisation graphique de prototypes personallisés.
Dans le cas où on s'interesse à la modélisation 2D on peut utiliser **Inskape** et si on veut faire plutôt de la modélisation 3D on a 
le choix entre **FreeCAD** ou **OpenSCAD**.  
- OpenSCAD est un logiciel qui se base sur du codage et personnellement je le trouve assez simple et amusant à utiliser. L'avantage dans
l'utilisation de ce logiciel ce trouve dans la possibilité de parametriser nos prototypes, mais ne vous inquiétez pas je vais vous montrez.  
- FreeCAD par contre est un peu plus complexe ce qui le rend plus intéressant à utiliser dans le cas où on a des modèles complexes à faire.  
Personnellement je n'ai pas encore eu la nécessité d'utiliser FreeCAD et pour cela ce module sera plus centré sur l'utilisation de OpenSCAD.

## Flexlinks 
 
Le but de ce module est de mettre en pratique ce qu'on a appris sur la modélisation et imprimer un [Flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks) 
de façon parametriser. Les flexlinks sont des pièces très simples qui permettent des mechanismes flexibles ou bistables, exploitables sur des 
(roulement de tambour...) LEGOs!!

## Modelisation
Avant de commencer je vous conséille de donner un coup d'oeil au [cheat sheet](https://openscad.org/cheatsheet/), ça vous aidera énormèment.
Au départ mon idée était de me mettre d'accord avec d'autres gens pour construire une catapulte dont moi je devais construire la base. J'ai fait
le modèle mais je me suis dit que pour ce module le but c'était aussi de costruire un Flexlinks donc j'ai ouvert un autre fichier et j'ai commencé
à y travailler. J'ai choisi de faire quelque chose qui ne me semblait pas trop compliqué, mais qu'au final m'a pris plus de temps que prévu. 
J'ai codé le modèle mais après avoir fini je me suis rendu compte qu'il me manquait deux choses:
- je n'ai pas beacoup chipoté avec la fonction module, ce qui au final me parassait dommage car ça rend le code plus lisible;
- j'ai fait une parametrisation mais ça permettais seulement de changer la taille du prototype en total et pas les dimensions de chaque partie 
de la pièce isolée.  
Je vous montre d'abord la pièce  

![](images/prototype.png)  

Au début le code était comme ça  

![](images/codebrouillon.png)  

Et ici je te présente la version finale  

![](images/codefinale.png)  

Comme vous pouvez voir dans la première version il y a trop de chiffres un peu partout ce qui rend le changement de parties de la pièce plus 
compliqué. Dans la deuxième version c'est aussi plus facile de comprendre ce qu'on fait grâce à l'utilisation des modules et à l'utilisation 
des variables à la place des valeurs.
Regardons ensemble le code.  
j'ai construit trois modules:
- la barre, étant simplement un cube centré et ensuite translaté sur l'axe des x;  
- le bout, qui sera la partie à attacher aux LEGOs que j'appelle "attachement". Ceci sera formé par deux cylindres fusionnés par la fonction **hull**;
- les trous qui seront d'office deux pour chaque cotés, formés aussi de deux cylindres de rayon et distance qui peuvent être reglés. 
Au début j'avait fait l'erreur de d'abord faire la différence entre attachement et le trou pour ensuite rajouter la barre et ça donnait la pièce que je 
voulais, mais avec un bout de la barre qui se retrouvait dans les trous. Donc j'ai corrigé ça en faisant l'union entre la tête et la barre pour ensuite 
faire la différence avec le trou, ce qui nous assure qu'il n'y aura pas un millimètre de barre dans les trous.
Puis j'ai construit une partie de la pièce se retrouvant après en superposition avec son image opposé, ce qui donne bien une pièce à deux têtes.  

## Catapulte  

Pour ce qui est la catapulte l'idée venait de ce [site](https://www.cgtrader.com/free-3d-print-models/science/astronomy-physics/physics-catapult) et 
l'idée était de prendre ce modèle et juste rendre la cuillère flexible. Ma partie était juste la base avec les deux plaques latérales. Le résultat étant  

![](images/cata.png)  

Le trou nous permettant d'insérer la cuillère et le cylindre horyzontal qu'on mettra horyzontallement pour regler la portée.  
Dans ce cas je ne trouvais pas l'intérêt à devoir parametriser chaque aspet de la pièce, donc le seul parametre inseré est la dimension de cette dernière.
Cela a rendu le code plus simple et court comme montré dans l'immage ci dessous.  
![](images/code.png)  
Le modèle peut être améliorer vu qu'il y a pas mal de matèriel utilisé mais qui n'est pas nécessaire, donc j'ai juste changé les dimensions et supprimé
les parties excédentaires, pour avoir ce code comme résultat (se trouvant aussi dans mon gitlab).  
![](images/codecata2.png)  
![](images/cata2.png)  

# Creative Common Licenses  

Les licences Creative Commons sont un ensemble de licences qui permettent de partager un travail que l'on a produit tout en conservant certains droits. 
Ces droits concernent l'utilisation et la publication de notre travail par des tiers. À travers ces licences, on peut renoncer à certains droits exclusifs 
qui sont normalement accordés au créateur. Effectivement, elles nous permettent de mettre des conditions sur l'utilisation, la modification ou la création 
d'œuvres dérivées à partir de la nôtre. Pour cela, il existe plusieurs types de licences :  

- Attribution (CC BY) : cela permet à d'autres de distribuer, modifier et utiliser notre travail (même à des fins commerciales), à condition d'être crédité.

- Attribution-ShareAlike (CC BY-SA) : cette licence est similaire à la première et exige en plus que les œuvres dérivées aient une licence similaire ou 
identique à la nôtre.  

- Attribution-NoDerivs (CC BY-ND) : cette licence permet la diffusion commerciale de l'œuvre à condition qu'il n'y ait pas de modification ou de création 
dérivée.  

- Attribution-NonCommercial (CC BY-NC) : cette licence permet aux autres de distribuer, modifier et utiliser l'œuvre, à condition que cela soit à des fins 
non commerciales et que le créateur original soit crédité.  

- Attribution-NonCommercial-ShareAlike (CC BY-NC-SA) : cette licence permet la distribution, la modification et l'utilisation non commerciale de l'œuvre, 
à condition que les œuvres dérivées soient distribuées sous une licence identique ou similaire à celle de l'œuvre originale.  

- Attribution-NonCommercial-NoDerivs (CC BY-NC-ND) : cette licence est la plus restrictive et permet uniquement la redistribution non commerciale de 
l'œuvre, à condition qu'elle soit distribuée dans sa forme originale et sans modification.  

# Checklist des tâches du module  

- [x] parametrically model some FlexLinks using 3D CAD software  
- [x] show how you did it with words/images screenshots  
- [x] include your original design files  
- [x] include the CC license to your work  
- [x] documente how you used other people's work and gave proper credit to complete your kit.  


