/*
Nom fichier: "Catapult"

Copyright (c) [2023], Sami El Hamdou

Ce projet est sous licence Creative Commons Attribution-ShareAlike [Attribution-ShareAlike 4.0 International]
Pour voir une copie de cette licence, visitez https://creativecommons.org/licenses/by-sa/4.0/legalcode
*/

side = 10;
$fn=100;
difference(){
    cube([side*8,side*7,side*0.5], center = true); 
    translate([3*side,0,0]) cube([0.5*side,0.5*side,1*side], center = true);
    cube([4*side,3*side,2*side], center=true);
    translate([-3.4*side,0,0]) cube([3*side,8*side,1*side], center = true);
    translate([3.5*side,2.5*side,0]) cube([2*side,3*side,1*side], center = true);
    translate([3.5*side,-2.5*side,0]) cube([2*side,3*side,1*side], center = true);
}
difference(){union(){
translate([0,2.5*side,2.5*side]) cube([side*1.5,side*0.5,side*5], center = true);
translate([0,-2.5*side,2.5*side]) cube([side*1.5,side*0.5,side*5], center = true);};
for(i=[1:4]){translate([0,-3*side,i*side]) rotate([0,90,90]) cylinder(h=6*side, r=0.3*side);}}