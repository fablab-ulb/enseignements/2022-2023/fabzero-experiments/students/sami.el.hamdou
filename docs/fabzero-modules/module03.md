# 3. Impression 3D

## Slicer
C'est le moment d'imprimer!! Mais il faut faire encore des choses avant... D'abord on télécharge un slicer dans notre pc. 
Ceci est un logiciel qui donne des instruction précise à l'imprimante sur la manière dont le prototype sera imprimé et 
pour cela il faudra passer du format STL (OpenSCAD) au format g-code (Slicer).
Le slicer qu'on utilisera est un logiciel open-source et gratuit, appelé [PrusaSlicer](https://help.prusa3d.com/article/install-prusaslicer_1903) 
(pour plus d'infos clique [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md).  
Si on ouvre notre base de catapulte (ce que j'ai imprimer au final) avec ce logiciel on a ça  
![](images/g-code.png)  
Il faudra donner un coup d'oeil au "Réglages d'impression" avant d'imprimer et on remarque différents choses qui pourraient être reglés:  
- **Hauteur de couche**: étant la hauteur de chaque couche imprimé  
- la **paroi** ou **coque** définissent les carachtéristiques de la paroi exterieur de la pièce  
- dans **remplissage** on peut changer la densité (ça defini la solidité du matériel), le motif (ça defini la forme du remplissage) et autres chose plus avancés  
- **jupe** et **bordure**:le prémier va definir le périmetre dans lequelle sera imprimé notre pièce et le deuxième va imprimer une bordure du coup, qui 
mantiendra la pièce bien stable sur la surface.  
Le reste n'étant pas intéressant dans mon cas.  
C'est le moment de vérifier aussi les **reglages du filament**. Ce qui est surtout important à vérifier ici, c'est la temperature de buse et du plateau 
car sinon l'impression ne va pas bien se derouler. Comme temperature j'ai retrouvé qu'ils étaient respectivement à 215°C et 60°C, ce qui est parfait.  
Personnellement je n'ai pas du faire des modifications sauf pour les dimensions du prototype: l'idée était d'imprimer une pièce de 10x6.7x3.5 mais il fallait
2h16m ce qui m'a montré une des limitations de l'impression 3D. Pour une pièce aussi pétite il fallait plus que 2h!! Donc j'ai diminué les dimensions du 
prototype pour rester dans les 20 minutes. Pour cela je suis passé à 4x2.7x1.4 pour un temps estimé de 15 minutes. C'est le moment d'exporter en g-code et inserer
 la clé avec le fichier dans l'imprimante.  
 
## Impression du prototype
 
 Et finalement on a notre prototype.  
 ![](images/photo.jpg)  
 Ceci était le prototype initial qui ensuite a été changé pour optimiser le temps d'impression de la pièce et pour avoir ça comme résultat  
 ![](images/cata.jpg)  
 Avec l'aide de [Kamel](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/), 
[Serge](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kodjo.dao/) et 
[Mariam](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/) on a complété notre kit et 
le résultat étant représenté ci-dessous  
 ![](images/catp.jpg)  
et vous pouvez trouver une démonstration de son fonctionnement [ici](https://www.youtube.com/shorts/9h-gW8e1B0o)  
Le seul problème étant l'épaisseur de l'axe de la cuillère qui n'est pas suffisante pour avoir une bonne puissance de lancé 
et donc on a rajouter un élastique pour un aider la cuillère. Plus loin on essaiera de reimprimer la cuillère 
avec la bonne épaisseur et peut être aussi de rajouter des mécanismes plus complexe pour compléter la catapulte.  

# Checklist des tâches du module  

- [x] Explaine what you learned from testing the 3D printers  
- [x] Explaine how you have identified your design parts parameters  
- [x] Show how you made your kit with words/images/screenshots
- [x] Include your original design files for 3D printing (both CAD and generic (STL, OBJ or 3MF) files)
- [x] Included images and description of your working mechanism.