# 1. Documentation  

Commençons par la création de notre site où nous mettrons la documentation de notre projet en utilisant le système de contrôle de version Git. Cela nous permettra 
non seulement de revenir ici pour revoir les étapes effectuées tout au long de cette expérience en cas d'oubli, mais aussi d'aider les prochains qui travailleront 
sur ce projet. En effet, dans notre documentation, nous ne mettrons pas seulement nos succès, mais aussi nos échecs dans la réalisation. Pour ce module, j'ai eu 
la chance de travailler avec [Kamel](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/) et 
[Serge](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kodjo.dao/), ce qui m'a permis de compléter le module sans problème.  

## Pourquoi git?

Git nous permet de travailler avec des collaborateurs sur le même projet, nous permettant ainsi de contrôler tous les changements apportés et de revenir en arrière 
en cas de problèmes de codage. Cela est possible grâce à sa capacité à sauvegarder les modifications sous de nouvelles versions tout en conservant les anciennes. 
Un autre avantage intéressant est la possibilité de sauvegarder une copie de notre projet sur notre ordinateur, ce qui est utile pour construire une sorte de 
brouillon que nous pouvons envoyer de nouveau au serveur lorsque tout est prêt. Si l'on ajoute le fait que Git est un logiciel open-source, on peut facilement 
comprendre pourquoi Git est un outil très puissant.  

## Git 

De nos jours, les dispositifs tels que les tablettes, les ordinateurs et les téléphones portables sont utilisés par tout le monde à travers une interface graphique. 
Cela implique l'interaction avec le système à l'aide d'éléments visuels tels que des icônes, des fenêtres, etc. appelée interface utilisateur graphique (GUI). 
Toutefois, pour une communication plus rapide et efficace avec notre ordinateur, nous utiliserons l'interface en ligne de commande (CLI). En écrivant simplement 
des commandes pour effectuer les opérations, Git BASH, une CLI similaire à Unix au niveau des commandes, nous permettra de travailler sur notre terminal.
L'installation de Git est possible en accédant simplement au [site](https://git-scm.com/). Les utilisateurs de Windows peuvent télécharger l'application en cliquant 
sur le bouton "Download for Windows" et en patientant un peu. Après avoir terminé l'installation, nous pourrons copier notre projet sur notre ordinateur pour 
effectuer des modifications en local avant de les renvoyer au serveur distant. Les commandes nécessaires avec Git BASH sont les suivants:

![](images/username.png)
![](images/email.png)
![](images/check-configuration.png)  
Le dernier nous permettant de vérifier que tous les changements ont été effectués.  

## Clé SSH

Corrige moi les fautes de grammaire dans ce texte "J'ai dû faire des recherches pour comprendre le fonctionnement d'une clé SSH, voici ce que j'ai appris (et pour 
ceux qui ont des difficultés à comprendre). N'oubliez pas que nous avons une copie de notre projet sur notre appareil, ce qui nous permet de faire des modifications 
en toute sécurité avant de tout envoyer sur le serveur. Maintenant, il nous manque la possibilité de communiquer avec le serveur en toute sécurité. La clé SSH nous 
permettra de nous connecter au serveur à distance sans avoir à saisir le nom d'utilisateur et le mot de passe à chaque fois. Les étapes suivantes nous permettront 
de créer une **clé publique** et une **clé privée** (qui ne doit pas être partagée).
Lorsque nous tentons de nous connecter au serveur à distance, notre appareil envoie notre clé publique au serveur, qui utilise cette dernière pour créer un message 
crypté. Ce message ne peut être déchiffré qu'avec la clé privée. En fin de compte, ce n'était pas si compliqué, n'est-ce pas?
Comme vous pouvez le constater, il s'agit d'un outil très puissant pour communiquer avec un serveur à distance en toute sécurité.
Pour commencer, nous devons créer cette clé. Nous ouvrons notre Git BASH et saisissons les commandes suivantes:

![](images/generationclé.png)  

ED25519 étant la méthode de cryptage conseillé à utiliser.  
Dès qu'on appui sur "envoie" git BASH nous demande d'insérer une "passphrase" qui est essentiellement un mot de passe et après qu'on aura confirmé le mot de passe, 
on arrivera à avoir notre paire de clé.  
À ce moment on va sauvegarder notre clé publique dans le serveur à distance, pour ce faire il faudra d'abord copier la clé publique qui peut ce
 faire en exploitant git BASH
![](images/copieclépublique.png)  
Ensuite il faudra aller sur gitlab. On clique en haut à droite sur notre profile et puis sur "preferences"  
![](images/gitlabclé.png)  
À gauche de la pages on va cherchez "SSH keys", ce qui nous affichera cette page  
![](images/gitlabssh.png)  
Ici on mettra notre clé ssh publique et ensuite on fait un test pour voir si tout est bon  
![](images/cloningssh.png)  
C'est le moment de cloner notre projet, donc on revient à la page de notre projet sur gitlab, on clique sur le bouton bleu "clone"
et ensuite on copie l'URL  
![](images/clonage.png)  
par la suite on tape sur git BASH "git clone", on colle ce qu'on a copié tout à l'heure et on envoie  
![](images/copie.png)  
Finalement on a cette fameuse copie dans notre pc, donc on est prêt pour commencer notre documentation. Il faut savoir qu'il y a
quattre commandes essentiels pour sauvegarder nos modifications dans le serveur à distance:   

![](images/gitpulls.png)  
- "git pull" nous permettra de recuperer et intégré avec un autre référentiel ou une branche locale 
![](images/gitadds.png)  
- "git add" y rajoute toutes les modifications faites dans le cas où on raoute un asterisque, sinon il faudra spécifier quelle fichier il doit mettre à jour  
![](images/gitcommits.png)  
- "git commit" nous permet d'y rajouter aussi un petit message pour expliquer les modifications faites
![](images/gitpushs.png)  
- "git push" renvoie les changements au serveur   

Assurez-vous toujours de vous retrouver dans votre fichier "prenom.nom" quand vous utilisez ces commandes. Pour ce faire il vous faudra 
ecrire sur git BASH "cd nom du fichier" pour rentrer dans le fichier ou "cd .." pour revenir en arrière.

# Ecrire notre documentation

Finalement il est temps d'écrire notre documentation et normalement cela ce fait en HTML mais on utilisera plutôt le format 
Markdown, étant plus simple et intuitif. Donc on commence par télecharger un éditeur de texte et j'ai choisi Notepad++.  
On peut déjà faire des modifications du site en ouvrant le fichier **mkdocs.yml** où il y aura pas mal de choses à changer.

## Changement de theme
Pour le thème normalement il faudrait simplement regarder dans "names" sous "theme" et modifier le nom de notre thème comme dans l'image ci-dessous.  

![](images/mkdocsyml-windmilldark.png)  

Il y a différentes suggestion mais si vous voulez, vous pouvez ajouter un thème qui n'est pas dans la liste
Grâce à Serge et Kamel j'ai decouvert un thème qui m'intéressait et pour "l'équiper" je suis 
rentrer dans le fichier **requirements** et j'ai ajouté "mkdocs-windmill-dark": dark windmill étant le thème désiré.  

![](images/requirements.png)  

Ensuite on sauvegarde, on va sur git BASH et on envoie nos modifications au serveur 
(rappelez-vous, on fait "git pull", "git add", "git commit -m" et "git push"). Il nous manque juste une chose pour avoir notre super 
thème: sur git BASH il faudra écrire cette comande  

![](images/windmill.png)  

et après une rélativement longue réponse de la part de git BASH on a finalement notre nouveau thème, let's go!!

## Insertion des photos

Pour introduir des photos il faut d'abord ajouter ces derniers dans le fichier **images** (pour l'**index** il faudrait simplement ajouter dans **docs** -> **images**, 
sinon dans le fichier **docs** -> **fabzero-modules** -> **images**) et ensuite on peut aller dans l'éditeur de texte et écrire:  

![](images/image.png)  

(PS. Faites toujours attention à écrire le bon format)  

Pour modifier les images j'ai télechargé GraphicsMagick qui me permettra d'exploiter BASH pour faire des changements. Pour l'instant j'ai utilisé juste deux commandes:  
 
![](images/modificationimage.png)  

Pour vous montrer un exemple du résultat je vous montre une image de taille 735x1537  

![](images/funny.jpg)  

et maintenant je vous montre la même image que j'ai retraici à 277x512 grâce à la commande que je vous ai montré tout à l'heure  

![](images/smallfunny.jpg)  

Voilà, beaucoup mieux!  
Une autre commande que j'ai trouvé intéressante étant celui-là  

![](images/stripphoto.png)  

ça permet de mettre deux images l'une à côté de l'autre comme j'ai fait avec les photos du radeau dans mon introduction. 

## Management de projet  

Pour ce cours l'idée est d'y travailler quand j'ai du temps libre pendant la semaine et appliquer le développement en spirale. 
Il m'est déjà arriver de travailler à fond au début d'un projet ou d'un article ou même simplement quand j'étudie mais pour finalement 
me retrouver à avoir bien travailler que la première partie. Voilà pourquoi ça m'a beaucoup intéressé 
le développement en spirale qui consiste tout simplement à commencer le projet faisant le strict minimum et ensuite 
compléter tous les détails qui manquent avec le temps. Cela permettant de compléter le projet tout en ayant un dévellopement linéaire 
dans son entièreté.  

# Checklist des tâches du module  

Things we have done:
- [x] made a website and describe how you did it  
- [x] introduced yourself  
- [x] documented steps for uploading files to the archive  
- [x] documented steps for compressing images and keeping storage space low  
- [x] documented how you are going to use project management principles  
- [x] pushed to the class archive  
