## Foreword
Hi visitor!  
My name is Sami, I’m a student in BA3 of bioengineering following the 2022-2023 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication".
I'm italian with Moroccan origins. I came in Belgium four years ago and started the university even if it was a new language for me.  
When I'm not studying I'm on a basketball court or in the gym or even working. If I'm not feeling to be productive you can find me on my bed just watching 
some TV series or films.


![](images/avatar-photo.jpg)

Visit this website to see my work!

## My background
I was born in a nice city called Latina. I love Latina because it's not too big as a city, it's near to Rome and at the same time to the sea. 
This means that I could get a nice tan during the summer and go to Rome when I wanted to change a little bit. But even staying in my city wasn't bad, 
just chilling with my friends. For exemple every 15th august we made a barbecue on the beach and spent the night all together just talking, playing or swimming 
in the middle of the night.

## Previous work

I don't really have interesting work to expose except the project that I'm working on this year which I'm really proud of. The project is, making it simple, 
about building a raft with some plants on it that will absorb heavy metals in water (for instance ponds or lakes).  
![](images/strip.jpg)  
Till now it is going well, knowing that the raft is floating and the plants are well protected. We're still working on it in order to 
find the quantity of heavy metals absorbed by the three different species of plants that we put on the raft.

